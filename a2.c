
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <stdlib.h>

int portnumber;

// Types of Message
#define HELLO							1
#define HELLO_ACK						2
#define LIST_REQUEST					3
#define CLIENT_LIST						4
#define CHAT							5
#define EXIT							6
#define ERROR_CLIENT_ALREADY_PRESENT	7
#define ERROR_CANNOT_DELIVER			8

#define STAT_NOT_INIT					0
#define STAT_ACTIVE						1
#define STAT_INACTIVE					2

#pragma pack(1)

// struct of client information maintained in server
typedef struct CLIENT {
	int fd;
	struct sockaddr_in addr;
	char clientId[20];
	int status;
}CLIENT;

// message struct
typedef struct msg{
	short type;
	char src[20];
	char dst[20];
	int length;
	int msg_id;
	char data[0];
}msg;

// variable of clients
CLIENT client[FD_SETSIZE];
int clientNum = 0;
char recvbuf[FD_SETSIZE][512];
fd_set rset, allset;

// values for select
int i, maxi, maxfd, sockfd, sin_size, nready, n, fd_exist;

void showMsg(msg* m)
{
    int i, length;
    switch(ntohs(m->type)) {
        case HELLO:
            printf("HELLO ");
            break;
        case HELLO_ACK:
            printf("HELLO_ACK ");
            break;
        case LIST_REQUEST:
            printf("LIST_REQUEST ");
            break;
        case CLIENT_LIST:
            printf("CLIENT_LIST ");
            break;
        case CHAT:
            printf("CHAT ");
            break;
        case EXIT:
            printf("EXIT ");
            break;
        case ERROR_CLIENT_ALREADY_PRESENT:
            printf("ERROR_CLIENT_ALREADY_PRESENT ");
            break;
        case ERROR_CANNOT_DELIVER:
            printf("ERROR_CANNOT_DELIVER ");
            break;
        default:
            printf("UNKNOWN_TYPE %d ", m->type);
            break;
    }
    printf(" from %s to %s, len: %d, msg_id: %d ", m->src, m->dst, ntohl(m->length), ntohl(m->msg_id));
    length = ntohl(m->length);
    if(length != 0) {
        for(i = 0; i < length; i++) {
            putchar(m->data[i]);
        }
    }
    printf("\n");
}

// create a message with these parameters.
// note : be sure the parameter is valid.
msg* createMsg(short type, char* src, char* dst,
				int len, int msg_id, char* dat)
{
    int i = 0, length;
    
	msg *m = (msg*)malloc(sizeof(msg) + len);
	m->type = htons(type);
	strcpy(m->src, src);
	strcpy(m->dst, dst);
	m->length = htonl(len);
	m->msg_id = htonl(msg_id);
	if(dat != NULL) {
		for(i = 0; i < len; i++)
            m->data[i] = dat[i];
	}

	return m;
}

// sned client list to client. Client Info are filled in CLIENT_INFO array.
// length of dat equals sizeof(CLIENT_INFO) * clientNum;
int sendBackClientList(int clientIdx, char* dst)
{
	int re = 0, i, j;
	msg* m;
	printf("clientNum : %d\n", clientNum);
    char* dat = (char*)malloc(20 * clientNum);
    dat[0] = 0;

	for(i = 0, j = 0; i < FD_SETSIZE; i++) {
		if(client[i].status == STAT_ACTIVE) {
			strcat(dat, client[i].clientId);
			strcat(dat, "\n");
            j++;
            if(j == clientNum)
                break;
		}
	}

	m = createMsg(CLIENT_LIST, "Server", dst, strlen(dat), 0, (char*)dat);
	showMsg(m);
	re = send(client[clientIdx].fd, m, sizeof(msg) + strlen(dat), 0);
	
	return re;
}

int SendBackMsg(int clientIdx, short type, char* src, char* dst, int len, int msg_id, char* data)
{
	int re = 0;
	msg* m = createMsg(type, src, dst, len, msg_id, data);
    showMsg(m);
	re = send(client[clientIdx].fd, m, sizeof(msg) + len, 0);
	return re;
}

// close client socket. delete its record in client[]. decrease clientNum.
void closeClient(int clientIdx)
{
    printf("In Close Client\n");
	close(client[clientIdx].fd);
	FD_CLR(client[clientIdx].fd, &allset);
	client[clientIdx].fd = -1;
	client[clientIdx].status = STAT_NOT_INIT;
	client[clientIdx].clientId[0] = 0;
	memset(&client[clientIdx].addr, 0, sizeof(struct sockaddr_in));
	clientNum --;
	if(clientNum < 0)
        clientNum = 0;
}

// if get Message, then process it
void processMsg(int clientIdx)
{
	msg* m = (msg*)recvbuf[clientIdx];
    int length = ntohl(m->length);
    int msg_id = ntohl(m->msg_id);
    int type = ntohs(m->type);
	int re = 0, i;

    showMsg(m);
    
    // if length of packet header is not equals with length of actual data
    if(length != 0 && length != (strlen(m->data) + 1)) {
        printf("because of length error %d vs %d\n", length, strlen(m->data));
        closeClient(clientIdx);
        return;
    }


	switch(client[clientIdx].status) {
		case STAT_NOT_INIT:
            for(i = 0; i < FD_SETSIZE; i++) {
                if(client[i].fd != -1 && strcmp(client[i].clientId, m->src) == 0 && client[i].status == STAT_ACTIVE) {
//                    printf("SEND ERROR_CLIENT_ALREADY_PRESENT to\n");
                    re = SendBackMsg(clientIdx, ERROR_CLIENT_ALREADY_PRESENT, "Server", m->src, 0, msg_id, NULL);
                    closeClient(clientIdx);
                    if(re = 0) {
                        printf("send error with ERROR_CLIENT_ALREADY_PRESENT\n");
                    }
                    return;
                }
            }

			if(type == HELLO && (strcmp(m->dst, "Server") == 0) && (length == 0) && (msg_id == 0)) {
				strcpy(client[clientIdx].clientId, m->src);
				re = SendBackMsg(clientIdx, HELLO_ACK, "Server", client[clientIdx].clientId, 0, 0, NULL);
				if(re == 0) {
					printf("send error with %d [%d]\n", HELLO_ACK, 0);
					closeClient(clientIdx);
				}
				else if(fd_exist == 0){
//                    printf("SUCCESS HELLO_ACK\n");
					//int sendBackClientList(int clientIdx, char* dst)
					client[clientIdx].status = STAT_ACTIVE;
					clientNum++;
					sendBackClientList(clientIdx, m->src);
				}
				else {
                    client[clientIdx].status = STAT_ACTIVE;
					sendBackClientList(clientIdx, m->src);
				}
			}
			else {
				// not established clients
				closeClient(clientIdx);
			}
			break;
		case STAT_ACTIVE:
			if((strcmp(m->dst, "Server") == 0) && (type == LIST_REQUEST) && (length == 0) && (msg_id == 0)) {
				// LIST_REQUEST
				printf("CLIENT REQUEST LIST\n");
				re = sendBackClientList(clientIdx, m->src);
			}
			else if((strcmp(m->dst, "Server") == 0) && (type == EXIT) && (length == 0)) {
				// Exit
				printf("CLIENT EXIT\n");
				for(i = 0; i < FD_SETSIZE; i++) {
					if(strcmp(client[i].clientId, m->src) == 0) {
						closeClient(i);
						break;
					}
				}
			}
			else if((strcmp(m->dst, "Server") != 0) && (type == CHAT) && (strcmp(m->dst, m->src) != 0)) {
				// CHAT
				for(i = 0; i < FD_SETSIZE; i++) {
					if(strcmp(client[i].clientId, m->dst) == 0) {
						re = SendBackMsg(i, CHAT, m->src, m->dst, length, msg_id, m->data);
						if(re == 0) {
							printf("error in chat\n");
						}
						break;
					}
				}
				if(i == FD_SETSIZE) {
					re = SendBackMsg(clientIdx, ERROR_CANNOT_DELIVER, "Server", m->src, 0, msg_id, NULL);
					if(re == 0) {
						printf("error in not deliver error\n");
					}
				}
			}
			else if(m->type == HELLO && (strcmp(m->dst, "Server") == 0) && (m->length == 0) && (m->msg_id == 0)) {
				re = SendBackMsg(clientIdx, ERROR_CLIENT_ALREADY_PRESENT, "Server", m->src, 0, 0, NULL);
				if(re == 0) {
					printf("error in not deliver error\n");
				}
                closeClient(clientIdx);
			}
			else {
                closeClient(clientIdx);
			}
			break;
        default:
            printf("Unknown stat\n");
            break;
	}
}

int main(int argc, char *argv[])
{
	int listenfd, connfd;
	struct sockaddr_in chiaddr, servaddr;

	sin_size = sizeof(struct sockaddr_in);

    // get port number
    if (argc != 2) {
        fprintf(stderr, "Usage:%s portnumber\n\a", argv[0]);
        exit(1);
    }
    if ((portnumber = atoi(argv[1])) < 0) {
        fprintf(stderr, "invalid portnumber\n\a");
        exit(1);
    }

	// establish socket
	listenfd = socket(AF_INET,SOCK_STREAM,0);
	if(listenfd == -1) {
		printf("Server Error, socket established error %d\n", (char*)strerror(errno));
		exit(1);
	}

	// set socket param
	int opt = SO_REUSEADDR;
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(portnumber);

	// bind socket to ip:port
	if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(struct sockaddr)) == -1) {
		fprintf(stderr, "Server Error, bind error:%s\n\a", strerror(errno));
		exit(1);
	}

	// get server address & show it.
	struct sockaddr_in localaddr;
	int serv_len = sizeof(struct sockaddr_in);
	int re = getsockname(sockfd, (struct sockaddr *)&localaddr, (socklen_t*)&serv_len);
	if(re == 0) {
		char serv_ip[20];
		inet_ntop(AF_INET, &localaddr.sin_addr, serv_ip, sizeof(serv_ip));
		printf("Server Started successfully, local addr : %s\n", serv_ip);
	}

	if(listen(listenfd,SOMAXCONN) == -1) {
		printf("listen error \n");
		exit(1);
	}

	// initialize select
	maxfd = listenfd;
	maxi = -1;
	for(i = 0; i < FD_SETSIZE; i++) {
		client[i].fd = -1;
		client[i].clientId[0] = 0;

	}
	FD_ZERO(&allset);
	FD_SET(listenfd, &allset);

	while(1) {
		struct sockaddr_in addr;
		rset = allset;
		nready = select(maxfd + 1, &rset, NULL, NULL, NULL);
//		printf("Select() break and the return num is %d. \n", nready);

		if(FD_ISSET(listenfd, &rset)) {
			// get a connection from new client.
			if((connfd = accept(listenfd, (struct sockaddr *)&addr, (socklen_t *) & sin_size)) == -1) {
				printf("accept error\n");
				continue;
			}

            // to make sure if you type Ctrl+C to interrupt a client. they other client would know you are inactive
            // instead of unexisted. If the same sock appear, It would use the original record place.
			fd_exist = 0;
			for(i = 0; i < FD_SETSIZE; i++) {
				if(connfd == client[i].fd) {
					fd_exist = 1;
					client[i].status = STAT_NOT_INIT;
					client[i].clientId[0] = 0;
					client[i].addr = addr;
					if(connfd > maxfd)
						maxfd = connfd;
					FD_SET(connfd, &allset);
					nready--;
					if(nready <= 0)
						break;
					break;
				}
			}

			for(i = 0; fd_exist == 0 && i < FD_SETSIZE; i++) {
				if(client[i].fd < 0) {
				// add socket connection to client without init it.
					client[i].fd = connfd;
					client[i].status = STAT_NOT_INIT;
					client[i].clientId[0] = 0;
					client[i].addr = addr;

					if(i == FD_SETSIZE)
						printf("too many clients\n");
					FD_SET(connfd, &allset);
					if(connfd > maxfd)
						maxfd = connfd;
					nready--;
					if(nready <= 0)
						break;
				}
			}
		}

		for(i = 0; i < FD_SETSIZE; i++) {
			if((sockfd = client[i].fd) < 0)
				continue;

			if(FD_ISSET(sockfd, &rset)) {
//				printf("recv from connect ");
				if((n = recv(sockfd, recvbuf[i], 512, 0)) == 0) {
					//connection closed from client, set the status to inactive
					client[i].status = STAT_INACTIVE;
//					printf("%d &\n", n);
					printf("a client named %s quit\n", client[i].clientId);
					FD_CLR(sockfd, &allset);

					close(sockfd);
				}
				else {
//					printf("%d \n", n);
					processMsg(i);
					// process msg;
				}

				if(--nready <= 0)
					break;
			}
		}
	}


	return 0;
}
